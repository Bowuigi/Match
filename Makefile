CC=cc
OPT=-Os
CFLAGS=-Wall -Wpedantic
DESTDIR=/usr/local

build:
	$(CC) $(OPT) $(CFLAGS) mgr.c match.c -o mgr
	$(CC) $(OPT) $(CFLAGS) test.c match.c -o test
	$(CC) $(OPT) $(CFLAGS) -DSMART_CASE mgr.c match.c -o mgri

debug:
	$(CC) $(CFLAGS) -ggdb mgr.c match.c -o mgr
	$(CC) $(CFLAGS) -DSMART_CASE -ggdb mgr.c match.c -o mgri
	$(CC) $(CFLAGS) -ggdb test.c match.c -o test

install: build
	cp mgr $(DESTDIR)/bin

test: build
	./test
