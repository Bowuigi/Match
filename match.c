/*
	Match: Simple (and tiny) string matching library

	(C) 2021 Bowuigi

	zlib license
	------------

	This software is provided 'as-is', without any express or implied
	warranty.  In no event will the authors be held liable for any damages
	arising from the use of this software.

	Permission is granted to anyone to use this software for any purpose,
	including commercial applications, and to alter it and redistribute it
	freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented; you must not
	claim that you wrote the original software. If you use this software
	in a product, an acknowledgment in the product documentation would be
	appreciated but is not required.
	2. Altered source versions must be plainly marked as such, and must not be
	misrepresented as being the original software.
	3. This notice may not be removed or altered from any source distribution.
*/

#include "match.h"

// Create a Match_State from a pattern, used for caching the pattern information
Match_State Match_ParsePattern(char *pattern) {
	// Check for a special character by comparing the previous token with a '%'
	bool nextIsSpecial = false;

	// Index and size of the place where the compiled pattern is stored
	size_t i = 0;
	size_t size = 10;

	// This is where the compiled pattern is stored
	struct Match_Char *chars = malloc(size*sizeof(struct Match_Char));

	// Start parsing
	while (*pattern) {
		// Reallocate the "chars" array if it is too small
		if (i>size) {
			size *= 2;
			chars = realloc(chars, size*sizeof(struct Match_Char));

			// Check for OOM
			MATCH_ERR(chars==NULL ,"Out of memory")
		}

		// Underscore is a no-op unless escaped with %
		if (*pattern == '_' && !nextIsSpecial) {
			pattern++;
			continue;
		}

		// Provide default values for every field
		chars[i].inverted = isupper(*pattern);
		chars[i].c = 0;
		chars[i].type = M_EXACT;
		chars[i].n = 1;
		chars[i].orig_n = 1;

		// % makes the next character special
		if (*pattern =='%' && !nextIsSpecial) {
			nextIsSpecial = true;
			pattern++;
			continue;
		}

		// What I mean by "special" is that it matches a specific group
		if (nextIsSpecial) {
			char repeat[4] = {0};
			size_t n = 0;

			// Parse the number of times to repeat (%200n would match 200 numbers)
			while (isdigit(*pattern)) {
				// It supports numbers up to 9999, if there is a bigger one it just skips past it (%279301n would match 2793 numbers)
				if (n < 4) {
					repeat[n] = *pattern;
					n++;
				}
				pattern++;
			}

			// Convert the string we just got into a number
			int num_times = strtol(repeat, NULL, 10);

			// Set it if it is bigger than zero, otherwise just match one (%0a would match one, for example)
			if (num_times > 0) {
				chars[i].n = num_times;
				chars[i].orig_n = num_times;
			}

			// Check which group should we match
			switch (tolower(*pattern)) {
				// Match anything
				case 'a':
					chars[i].type = M_ANY;
					break;
				// Only printable characters
				case 'c':
					chars[i].type = M_PRINT;
					break;
				// Alphanumeric characters
				case 'h':
					chars[i].type = M_ALPHANUM;
					break;
				// Letters (A-Z a-z)
				case 'l':
					chars[i].type = M_LETTER;
					break;
				// Numbers
				case 'n':
					chars[i].type = M_NUM;
					break;
				// Punctuation
				case 'p':
					chars[i].type = M_PUNCT;
					break;
				// Whitespace
				case 's':
					chars[i].type = M_SPACE;
					break;
				// Uppercase letters
				case 'u':
					chars[i].type = M_UPPER;
					break;
				// Hexadecimal numbers
				case 'x':
					chars[i].type = M_HEX;
					break;
				// Escaping whatever is next (%_ matches an underscore)
				default:
					chars[i].type = M_EXACT;
					break;
			}
		}

		// Prevent being special for the rest of the pattern
		nextIsSpecial = false;

		// Set the other required fields if the match is exact
		if (chars[i].type==M_EXACT) {
			chars[i].c = *pattern;
			chars[i].inverted = false;
		}

		pattern++;
		i++;
	}

	// Return what we just did as a nicely formatted struct
	return (struct Match_State){.c=chars, .length=i};
}

// Delete a Match State, rendering it useless but recovering memory
void Match_DeleteState(struct Match_State *S) {
	free(S->c);
	S->length = 0;
	S = NULL;
}

// Print a pattern to stdout (remember that this is slow because IO is slow)
void Match_PrintPattern(struct Match_State S) {
	// Show if case sentitivity is enabled
	if (!S.smart_case) {
		puts("Smart case enabled");
	}

	// Select everything on the pattern
	for (size_t i=0; i<S.length; i++) {
		printf("%d ", S.c[i].n);

		// Show if it is inverted
		if (S.c[i].inverted)
			fputs("Inverted ", stdout);

		// Show the type
		switch (S.c[i].type) {
			case M_ALPHANUM:
				fputs("Alphanumeric character", stdout);
				break;
			case M_ANY:
				fputs("Any character", stdout);
				break;
			case M_EXACT:
				printf("'%c' character", S.c[i].c);
				break;
			case M_HEX:
				fputs("Hex [0-F] character", stdout);
				break;
			case M_LETTER:
				fputs("Letter", stdout);
				break;
			case M_NUM:
				fputs("Number", stdout);
				break;
			case M_PRINT:
				fputs("Printable character", stdout);
				break;
			case M_PUNCT:
				fputs("Punctuation", stdout);
				break;
			case M_SPACE:
				fputs("Space", stdout);
				break;
			case M_UPPER:
				fputs("Uppercase character", stdout);
				break;
		}

		// Add an 's' at the end to make it plural if we match more than one thing
		if (S.c[i].n > 1) {
			putchar('s');
		}

		// Finally add a newline
		putchar('\n');
	}
}

static bool CharEQ(char c1, char c2, bool smart_case) {
	if (smart_case) {
		if (islower(c1)) return c1 == tolower(c2);
		return c1 == c2;
	} else {
		return c1 == c2;
	}
}

// Use a Match State (a compiled pattern) to match a string, automagically used by the Match_String function
Match_Result Match_Cached(Match_State S, char *string) {
	// Initialize to not-matching state by default
	Match_Result result = {
		.matches = false,
		.start = 0,
		.end = 0
	};

	// String and pattern lengths
	size_t str_len = strlen(string);
	size_t pat_len = S.length;

	bool matches = true;

	// Check if we should advance to next char in pattern, used for repetitions
	bool skip_char = false;

	// Current indexes of the string and the pattern
	size_t str_i = 0;
	size_t pat_i = 0;

	// Loop on the string
	for (; str_i < str_len; str_i++, skip_char = false) {
		// Check if the character matches
		switch (S.c[pat_i].type) {
			case M_ALPHANUM : matches = isalnum(string[str_i])                            ; break ;
			case M_ANY      : matches = true                                              ; break ;
			case M_EXACT    : matches = CharEQ(S.c[pat_i].c, string[str_i], S.smart_case) ; break ;
			case M_HEX      : matches = isxdigit(string[str_i])                           ; break ;
			case M_LETTER   : matches = isalpha(string[str_i])                            ; break ;
			case M_NUM      : matches = isdigit(string[str_i])                            ; break ;
			case M_PRINT    : matches = isprint(string[str_i])                            ; break ;
			case M_PUNCT    : matches = ispunct(string[str_i])                            ; break ;
			case M_SPACE    : matches = isspace(string[str_i])                            ; break ;
			case M_UPPER    : matches = isupper(string[str_i])                            ; break ;
		}

		// Invert match for special types in uppercase
		if (S.c[pat_i].inverted) matches = !matches;

		// If there are more repetitions to match but we run out of string then fail
		if (str_i >= str_len - 1 && S.c[pat_i].n > 1) break;

		if (matches == true) {
			// Check for repetitions
			S.c[pat_i].n--;
			if (S.c[pat_i].n > 0) skip_char = true;

			// Match completed
			if (pat_i >= pat_len - 1 && S.c[pat_i].n == 0) {
				result.end = str_i;
				result.matches = true;
				break;
			}
		}

		// Skip to next character if match fails
		if (matches == false) {
			skip_char = true;
			pat_i = 0;
			result.start++;
		}

		// Jump to next character in pattern if we are not on a repetition
		if (skip_char == false) pat_i++;
	}

	// Match completed
	if (result.matches == false && pat_i >= pat_len - 1 && S.c[pat_i].n == 0) {
		result.end = str_i;
		result.matches = true;
	}

	// Set to invalid values when no match is possible
	if (result.matches == false) {
		result.start = -1;
		result.end = -1;
	}

	// Reset pattern repetitions to their original values
	for (size_t i = 0; i < pat_len; i++)
		S.c[i].n = S.c[i].orig_n;

	// Exit point
	return result;
}

// Wrapper that parses the pattern given, matches it on the string, deletes the compiled pattern and returns the result
Match_Result Match_String(char *pattern, char *string, bool smart_case) {
	// S: Compiled pattern
	// R: Match result
	Match_State S = Match_ParsePattern(pattern);
	
	// Set smart case if required
	S.smart_case = smart_case;

	Match_Result R = Match_Cached(S, string);

	// Reclaim memory
	Match_DeleteState(&S);

	// Finally, return the result
	return R;
}
