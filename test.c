/*
	Test: Test Bowuigi/Match to see if it works properly

	(C) 2021 Bowuigi

	zlib license
	------------

	This software is provided 'as-is', without any express or implied
	warranty.  In no event will the authors be held liable for any damages
	arising from the use of this software.

	Permission is granted to anyone to use this software for any purpose,
	including commercial applications, and to alter it and redistribute it
	freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented; you must not
	claim that you wrote the original software. If you use this software
	in a product, an acknowledgment in the product documentation would be
	appreciated but is not required.
	2. Altered source versions must be plainly marked as such, and must not be
	misrepresented as being the original software.
	3. This notice may not be removed or altered from any source distribution.
*/

#include "match.h"
#include <stdio.h>

void Test_Match(char *pattern, char *string, bool should_match) {
	Match_State S = Match_ParsePattern(pattern);
	printf("Pattern: '%s'\nInterpretation:\n", pattern);
	Match_PrintPattern(S);
	Match_Result R = Match_Cached(S, string);
	printf("String to match: '%s'\nMatches? %s\nStart: %ld\nEnd: %ld\n", string, R.matches ? "yes" : "no", R.start+1, R.end+1);
	Match_DeleteState(&S);
	if (should_match != R.matches) { puts("Test failed"); } else { puts("Test passed"); }
	putchar('\n');
}

int main() {
	Test_Match("1234", "1234", true);
	Test_Match("1234", "12345678", true);
	Test_Match("1234", "012345678", true);
	Test_Match("%a%a%a%a%a world", "hello world", true);
	Test_Match("%n%n%n%n", "1234", true);
	Test_Match("%x", "abcdef1234", true);
	Test_Match("%p%p%p%p", ".,;:", true);
	Test_Match("WRONG", "RIGHT", false);
	Test_Match("%A", "that should fail on any condition", false);
	Test_Match("%5a", "should match five chars", true);
	Test_Match("%2a", "t", false);
	Test_Match("%C%c", "\027a", true);
}

