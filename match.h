/*
	Match: Simple (and tiny) string matching library

	(C) 2021 Bowuigi

	zlib license
	------------

	This software is provided 'as-is', without any express or implied
	warranty.  In no event will the authors be held liable for any damages
	arising from the use of this software.

	Permission is granted to anyone to use this software for any purpose,
	including commercial applications, and to alter it and redistribute it
	freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented; you must not
	claim that you wrote the original software. If you use this software
	in a product, an acknowledgment in the product documentation would be
	appreciated but is not required.
	2. Altered source versions must be plainly marked as such, and must not be
	misrepresented as being the original software.
	3. This notice may not be removed or altered from any source distribution.
*/

// Header guard
#ifndef MATCH_H
#define MATCH_H

// Compatibility
#ifdef __cplusplus
extern "C" {
#endif

// Include a bunch of libraries
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// Helper macro for when things go wrong
#define MATCH_ERR(cond, msg) \
	if ((cond)==true) {      \
		fprintf(stderr, "Match, fatal: %s", msg); \
		exit(1);             \
	}

// Groups to match
typedef enum Match_Type {
		M_ALPHANUM,        // Alphanumeric (A-Z a-z 0-9)
		M_ANY,            // Anything
		M_EXACT,         // Exact match
		M_HEX,          // Hexadecimal number (A-F a-f 0-9)
		M_LETTER,      // Letter (A-Z a-z)
		M_NUM,        // Number (0-9)
		M_PRINT,     // Printable character
		M_PUNCT,    // Punctuation
		M_SPACE,   // Whitespace
		M_UPPER,  // Uppercase letters
} Match_Type;

// The internal structure of a single character/group to match
typedef struct Match_Char {
	// The type of the match
	Match_Type type;
	// Shows if the match is inverted (everything except M_EXACT)
	bool inverted;
	// Character matched (only appears in M_EXACT)
	char c;
	// Number of times to repeat it
	int n;
	// Original value of the previous field, used for caching
	int orig_n;
} Match_Char;

// A compiled pattern
typedef struct Match_State {
	// A lot of the structures described above
	struct Match_Char *c;
	// The number of them on this structure
	size_t length;
	// If enabled, lowercase letters will match their uppercase variants too
	bool smart_case;
} Match_State;

// Result of the match
typedef struct Match_Result {
	bool matches;
	long start;
	long end;
} Match_Result;

// Every function on the library
Match_State Match_ParsePattern(char *pattern);
void Match_DeleteState(Match_State *S);
void Match_PrintPattern(Match_State S);
Match_Result Match_Cached(Match_State S, char *string);
Match_Result Match_String(char *pattern, char *string, bool smart_case);

// Compatibility
#ifdef __cplusplus
}
#endif

#endif // MATCH_H header guard
