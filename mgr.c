/*
	MGr: A grep like program used to demonstrate the capabilities of Bowuigi/Match

	(C) 2021 Bowuigi

	zlib license
	------------

	This software is provided 'as-is', without any express or implied
	warranty.  In no event will the authors be held liable for any damages
	arising from the use of this software.

	Permission is granted to anyone to use this software for any purpose,
	including commercial applications, and to alter it and redistribute it
	freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented; you must not
	claim that you wrote the original software. If you use this software
	in a product, an acknowledgment in the product documentation would be
	appreciated but is not required.
	2. Altered source versions must be plainly marked as such, and must not be
	misrepresented as being the original software.
	3. This notice may not be removed or altered from any source distribution.
*/

#include "match.h"
#include <stdio.h>
#include <errno.h>

// Using some macros to check if smart case matching is enabled at compile time because I am too lazy to add a command argument parser
#ifdef SMART_CASE
#define smart_case true
#else
#define smart_case false
#endif

int main(int argc, char **argv) {
	switch (argc-1) {
		default:
			fputs("MGr: search files or stdin using Bowuigi/Match patterns, usage: mgr pattern [file]\n", stderr);
			break;
		case 1: {
			char *line;
			size_t len;
			// New on POSIX, getline is pretty cool
			while (getline(&line, &len, stdin) != -1) {
				// Example of checking if something matches
				if (Match_String(argv[1], line, smart_case).matches)
					printf("%s",line);
			}
			break;
		}
		case 2: {
			errno = 0;
			FILE *fp = fopen(argv[2], "r");
			if (!fp) {fprintf(stderr, "MGr: Error while reading file, %s\n", strerror(errno));return 1;}

			char *line;
			size_t len;
			while (getline(&line, &len, fp) != -1) {
				if (Match_String(argv[1], line, smart_case).matches)
					printf("%s",line);
			}
			fclose(fp);
			break;
		}
	}
}
