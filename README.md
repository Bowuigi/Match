# Match

Simple (and tiny) pattern matching library in POSIX C99 inspired by the Lua string replacement library.

It is all on a single header and C source (match.h and match.c), just drop it on your project and include it

mgr.c and test.c are an example and the testing program specifically
Description
-----------

Uses syntax similar to Lua string.{find,match,sub,gsub,etc.}, the options are the following:

| Token |   Matches    |
|-------|--------------|
|  %a   |   Anything   |
|  %h   | Alphanumeric |
|  %l   |    Letters   |
|  %n   |    Numbers   |
|  %p   |  Punctuation |
|  %s   |    Spaces    |
|  %u   |   Uppercase  |
|  %x   |   Hex [0-F]  |
|  %c   |   Printable  |

If the tokens are in uppercase, then that match would be inverted, for example, %n matches numbers, but %N matches anything but numbers.

If the token doesn't start with % then it is just a normal exact match

If the token is %%, then it matches '%'

If there is a number between the '%' and the token, it is taken as the amount of times to match it, for example, "%4n" would match four numbers like "1234"

Usage
-----

There are two ways to match patterns:

- By caching the pattern to reuse it (good for matching a lot of strings with the same pattern)
- By calling a single function that does everything above for you but only a single time (good for matching only one string with one pattern)

The simplest is the second, because you only have to do:

```c
if (Match_String("pattern", "string to match on", smart_case_enabled).matches) {
	// This is executed when the pattern is matched on the string
} else {
	// This is ran otherwise
}
```

But since the other one is what you will most likely use, here is how:

```c
Match_State pattern = Match_ParsePattern("pattern");

pattern.smart_case = smart_case_enabled;

Match_Result result  = Match_Cache(pattern, "string to match it on");
Match_Result result2 = Match_Cache(pattern, "other string to match it on");
Match_Result result3 = Match_Cache(pattern, "yet another string to match it on");
...

Match_DeleteState(&S);
```

Remember to delete the compiled pattern with `Match_DeleteState` once you finished using it.

The smart case setting sets whether lowercase exact matches also match their uppercase variants, for example:

- Smart case disabled: `aA` matches `aA`
- Smart case enabled: `aA` matches `aA` and `AA`

If you have any doubts, the source code is very readable and has a lot of comments explaining the situation.

